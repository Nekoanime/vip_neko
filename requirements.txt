django==1.11
django-registration-redux
django-import-export
lxml
requests
django-rosetta
# https://github.com/fabric/fabric/archive/v2.zip
https://github.com/timonweb/django-bulma/archive/master.zip
django-recaptcha
django-loginas
django-constance[database]
django-constance
django-paypal
raven
