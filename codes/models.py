from __future__ import unicode_literals
import uuid
from django.db import models
from django.contrib.auth.models import User
from .manager import AccountCodeManager
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.utils import timezone
from .signals import show_me_the_money
from paypal.standard.ipn.signals import valid_ipn_received

CH = (
    ("free", "FREE"),
    ("prem30", "prem30"),
)


class Code(models.Model):
    code = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    type = models.CharField(max_length=10, choices=CH, default="prem30")
    exported = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u"{}".format(self.code)


class AccountCode(models.Model):
    code = models.OneToOneField(Code)
    account = models.ForeignKey(User)
    expired = models.DateField(null=True, blank=True, default=None)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = AccountCodeManager()


valid_ipn_received.connect(show_me_the_money)
