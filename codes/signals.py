from paypal.standard.models import ST_PP_COMPLETED
from constance import config
from django.contrib.auth.models import User
from django.utils import timezone
from decimal import Decimal
from django.core.mail import send_mail
from django.conf import settings
from django.template.loader import render_to_string

def show_me_the_money(sender, **kwargs):
    ipn_obj = sender
    # print("signal fired %s %s"% (ipn_obj.payment_status, ST_PP_COMPLETED) )
    # print( "email %s  %s " % (ipn_obj.receiver_email, type(ipn_obj.mc_gross)) )
    if ipn_obj.payment_status == ST_PP_COMPLETED:
        # WARNING !
        # Check that the receiver email is the same we previously
        # set on the `business` field. (The user could tamper with
        # that fields on the payment form before it goes to PayPal)
        if ipn_obj.receiver_email != config.paypal:
            # Not a valid payment
            return
        from .models import AccountCode, Code
        username = ipn_obj.custom
        code = Code.objects.create()
        user = User.objects.get(username=username)
        expired = timezone.now() + timezone.timedelta(days=config.expire_days)
        AccountCode.objects.create(account=user, code=code, expired=expired)

        subject = config.email_subject
        context = {'user':user, 'code': code,'paypal': ipn_obj }
        message = render_to_string("email.txt", context)
        to_emails = [ipn_obj.payer_email,]
        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, to_emails)




