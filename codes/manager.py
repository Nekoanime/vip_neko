from django.db import models
from django.utils import timezone

class AccountCodeManager(models.Manager):

    def active(self):
        now = timezone.now()
        self.filter(expired__lte=now)

