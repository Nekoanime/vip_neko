# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from .models import AccountCode, Code
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.utils import timezone
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
from paypal.standard.forms import PayPalPaymentsForm
from constance import config


class CodeRedirectMixin(object):
    redir_url = "products"
    def dispatch(self, request, *args, **kwargs):
        dis = super(CodeRedirectMixin, self).dispatch(request, *args, **kwargs)
        account = request.user
        qs = AccountCode.objects.filter(account=account)
        now = timezone.now() 
        qs = qs.filter(expired__gte=now)
        code_exists = qs.exists()
        if not code_exists:
            messages.warning(request, _(u"Expired code"))
            return HttpResponseRedirect(reverse_lazy(self.redir_url))
        return dis


class ProductView(LoginRequiredMixin, TemplateView):
    template_name="codes/show_products.html"

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        account = request.user
        qs = AccountCode.objects.filter(account=account)
        now = timezone.now()
        #qs = qs.filter(expired__lte=now)
        context["code_active"] = qs

        paypal_dict = {
            "cmd": "_xclick-subscriptions",
            'business': "%s" % config.paypal,
            "a3": "%s" % config.price,
            "p3": "1",
            "t3": "M",
            'src': "1",
            'sra': "1",
            'no_note': "1",
            "item_name": "Membresia VIP Subscripcion de nekoanimedd",
            "notify_url": self.request.build_absolute_uri(reverse("paypal-ipn")),
            "return": self.request.build_absolute_uri(reverse("home")),
            "cancel_return": self.request.build_absolute_uri(reverse("home")),
            "custom":self.request.user.username,
        }
        p = PayPalPaymentsForm(initial=paypal_dict)
        context["paypal"] = p

        context["paypal_once"] = PayPalPaymentsForm(initial={
            'business': "%s" % config.paypal,
            "amount": "%s" % config.price,
            "item_name": "Membresia VIP 1 mes de nekoanimedd",
            "notify_url": self.request.build_absolute_uri(reverse("paypal-ipn")),
            "return": self.request.build_absolute_uri(reverse("home")),
            "cancel_return": self.request.build_absolute_uri(reverse("home")),
            "custom":self.request.user.username,

        })
        return self.render_to_response(context)

    def create_code(self, code):
        code = Code.objects.get(code=code.strip())
        account = self.request.user
        acode, created = AccountCode.objects.get_or_create(code=code, defaults = dict(account=account))
        if created:
            acode.expired = timezone.now() + timezone.timedelta(days=config.expire_days)
            acode.save()
        return created

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        code = request.POST.get("code")
        try:
            created = self.create_code(code)
            if created:
                messages.info(request, _(u"Code accepted!"))
                return HttpResponseRedirect(reverse_lazy("home"))
            else:
                messages.warning(request, _(u"Code was used"))
        except (Code.DoesNotExist, ValueError):
            messages.error(request, _(u"Code not found"))
        return self.render_to_response(context)

product_view = ProductView.as_view()

