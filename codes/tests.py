from test_plus.test import TestCase
from codes.models import Code, AccountCode
from vip_neko.models import UserConf
from vip_neko.views import FetchMixin
from django.contrib.auth.models import User
from constance import config
from codes.signals import show_me_the_money
import uuid


class CodesTest(TestCase):
    url ="http://nekoanimedd.com/emitiendose/descargar-anime-boruto-naruto-next-generations-1080-65"
    def setUp(self):
        user = self.make_user('test')
        self.user = user
        UserConf.objects.create(user=user)

    def test_can_search(self):
        user = User.objects.get(username="test")
        f = FetchMixin()
        wordpress_ids = ( 19468, 19443, 19452, 19434, 19422, 19409, 19305, 19296, 19287,
            19278, 19269, 19256, 19247, 19238, 19229,) 
        for i in xrange(10):
            post_id = wordpress_ids[i]
            user_cansearch = f._check_user_can_search(user)
            res = f.fetch_dd(post_id)
            f._create_search(user, post_id, res)
        check = f._check_user_can_search(user)
        assert check, ("user can not creacke new one ", check)

    def test_fetch(self):
        f = FetchMixin()
        title, image, post_id = f.fetch(self.url)
        assert post_id
        assert title,title
        assert image, image

    def create_code(self):
        code = Code.objects.create()
        self.post('products', data=dict(code=code.code))
        self.response_302()
        self.get_check_200("home")
        return code


    def test_products(self):
        with self.login(username='test'):
            self.get("home")
            self.response_302()
            self.get_check_200("products")
            code = uuid.uuid4()
            self.post('products', data=dict(code=code))
            self.response_200()
            code = self.create_code() 
            self.post('products', data=dict(code=code.code))
            self.response_200()

    def test_home(self):
        with self.login(username='test'):
            self.create_code()
            self.post('home', data=dict(url=self.url))
            self.response_200()
            post_id = self.get_context("post_id")
            self.post("home", data=dict(post_id=post_id))
            self.response_200()

    def test_paypal(self):
        class MockPaypal:
            payment_status = 'Completed'
            receiver_email = config.paypal
            custom = 'test'
            payer_email = u'zodman@gmail.com'

        obj = MockPaypal()
        show_me_the_money(sender=obj)        

    def test_account_manager(self):
        with self.login(username='test'):
            self.create_code()
            AccountCode.objects.active()
