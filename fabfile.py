from fabric import task
from invoke import run as local



@task(hosts=["wp@url.info.gf",])
def deploy(ctx):
    with ctx.prefix("cd vip_neko && source ~/.virtualenvs/vip_neko/bin/activate"):
        ctx.run("git pull")
        ctx.run("pip install -r requirements.txt")
        ctx.run("python manage.py migrate")
        ctx.run("touch vip_neko/wsgi.py")


@task
def test(ctx):
    cmds = [
        "coverage run --source=.  manage.py test",
        "coverage report  ",
        "coverage html  ",
    ]
    for cmd in cmds:
        local(cmd, echo=True)

