from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView
from .views import home
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^rosetta/', include('rosetta.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^admin/', admin.site.urls),
    url(r'^admin/', include('loginas.urls')),
    url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^codes/', include('codes.urls')),
    url(r'^home/$',home, name="home"),
    url(r'^paypal/', include('paypal.standard.ipn.urls')),
    url(r'^$', TemplateView.as_view(template_name="base.html"), name='index'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

