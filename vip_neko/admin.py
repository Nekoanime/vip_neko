from django.contrib import admin
from .models import UserConf, UserSearch



class UserConfAdmin(admin.ModelAdmin):
    list_display = ("user", "limit")

class USAdmin(admin.ModelAdmin):
    list_display = ("user", "post_id", "created_at",)
admin.site.register(UserConf, UserConfAdmin)
admin.site.register(UserSearch, USAdmin)
