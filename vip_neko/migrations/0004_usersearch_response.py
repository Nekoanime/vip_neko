# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-13 12:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vip_neko', '0003_auto_20171011_1406'),
    ]

    operations = [
        migrations.AddField(
            model_name='usersearch',
            name='response',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
