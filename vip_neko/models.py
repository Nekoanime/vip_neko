from django.db import models
from django.contrib.auth.models import User
from registration.signals import user_registered
from django.dispatch import receiver

class UserConf(models.Model):
    user = models.OneToOneField(User)
    limit = models.PositiveIntegerField(default=15)
    
    def __unicode__(self):
        return u"{}".format(self.user)


class UserSearch(models.Model):
    user = models.ForeignKey(User)
    post_id = models.PositiveIntegerField()
    response = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


    class Meta:
        unique_together = ("user", "post_id")

    def __unicode__(self):
        return u"{}".format(self.user)


@receiver(user_registered)
def create_conf(sender, **kwargs): # pragma: no cover
    user =kwargs.get("user")
    _, user_conf = UserConf.objects.get_or_create(user=user)

