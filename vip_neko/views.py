from django.views.generic import TemplateView
from codes.views import CodeRedirectMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from lxml import html
import requests
from urlparse import urlparse, parse_qs
from .models import UserSearch, UserConf
from django.utils import timezone
import json
from django import forms
from captcha.fields import ReCaptchaField
import urllib2

class ValidForm(forms.Form):
    captcha = ReCaptchaField()



class FetchMixin:

    def fetch(self, url):
        
        s = html.parse(urllib2.urlopen(url))
        title = s.xpath("//title/text()").pop()
        image = s.xpath("//meta[@property='og:image']/@content").pop()
        post_url = s.xpath("//link[@rel='shortlink']/@href").pop()
        post_id =  parse_qs(urlparse(post_url).query).get("p").pop()
        return title, image, post_id

    def fetch_dd(self, post_id):
        re = requests.get("https://ctrl.anime-esp.com/ctrl/search/", params={'i': post_id})
        return re.json()

    def check_can_search(self):
        user = self.request.user
        return self._check_user_can_search(user)

    def _get_searches(self, user):
        today = timezone.now()
        return UserSearch.objects.filter(user=user, created_at__date=today.date())

    def _get_limit(self, user):
        uconf,_ = UserConf.objects.get_or_create(user=user)
        return uconf.limit

    def _check_user_can_search(self, user):
        limit = self._get_limit(user)
        count_search = self._get_searches(user).count()
        return  count_search <= limit


    def save_search(self, post_id, response):
        user = self.request.user
        return self._create_search(user, post_id, response)

    def _create_search(self, user, post_id, response):
        if self._check_user_can_search(user):
            us, _created = UserSearch.objects.get_or_create(user=user, post_id=post_id, defaults={'response':response})
            us.save()
            return us
        return None

class HomeView(LoginRequiredMixin, CodeRedirectMixin, TemplateView, FetchMixin):
    template_name="home.html"

    def get_context_data(self, **kwargs):
        ctx = super(HomeView, self).get_context_data(**kwargs)
        searches = self._get_searches(self.request.user)
        can_search = self.check_can_search()
        limit = self._get_limit(self.request.user)
        ctx.update({'searches': searches, 'url': self.request.GET.get("url"),
            'percent': searches.count()*100/(limit+1)*1.0,
            'can_search': can_search, 'LIMIT': limit +1 })
        return ctx

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        url = request.POST.get("url")
        post_id = request.POST.get("post_id")
        if post_id:
            res = None
            if self.check_can_search():
                res = self.fetch_dd(post_id)
                success = self.save_search(post_id, res)
                if not success:
                    res = None
            context.update({'url': url, 'res': res, 'post_id':post_id })
        elif url and "anime-esp.com" in url:
            title, image, post_id = self.fetch(url)
            context.update({'title': title, 'image': image, 'post_id': post_id, 
                'url':url})
        return self.render_to_response(context)

home = HomeView.as_view()
